package parqueadero;

import java.util.List;

import estructuras.Cola;
import estructuras.Pila;

import java.util.ArrayList;
import java.util.Date;


public class Central 
{

	/**
	 * Cola de carros en espera para ser estacionados
	 */
	private List<Carro> carrosEnEspera;

	/**
	 * Pilas de parqueaderos 1, 2, 3 .... 8
	 */
	private List<List> parqueaderos;


	/**
	 * Pila de carros parqueadero temporal:
	 * Aca se estacionan los carros temporalmente cuando se quiere
	 * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
	 */
	private List<Carro> parqueaderoTemporal;


	/**
	 * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
	 */
	public Central ()
	{
		carrosEnEspera = new Cola<Carro>();
		parqueaderos = new ArrayList<List>();
		for (int i = 0; i < 8; i++) {
			parqueaderos.add(new Pila<Carro>());
		}
		parqueaderoTemporal = new Pila<Carro>();
	}

	/**
	 * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
	 * @param pColor color del vehiculo
	 * @param pMatricula matricula del vehiculo
	 * @param pNombreConductor nombre de quien conduce el vehiculo
	 */
	public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
	{
		carrosEnEspera.add(new Carro(pColor, pMatricula, pNombreConductor));
	}    

	/**
	 * Parquea el siguiente carro en la cola de carros por parquear
	 * @return matricula del vehiculo parqueado y ubicaci�n
	 * @throws Exception
	 */
	public String parquearCarroEnCola() throws Exception
	{
		String rta = "";
		for (int i = 0; i < parqueaderos.size(); i++) {
			List<Carro> parAct = parqueaderos.get(i);
			if (parAct.size() < 4) {
				try {
					parAct.add(carrosEnEspera.remove(0));
					rta = parAct.get(parAct.size()-1).darIdentificador();
					break;
				} catch (Exception e) {
					System.out.println("No hay carros en espera.");
					return "\n";
				}
			}
		}
		if (rta.equals("")) {
			throw new Exception("El parqueadero está lleno");
		}
		return rta;
	} 
	/**
	 * Saca del parqueadero el vehiculo de un cliente
	 * @param matricula del carro que se quiere sacar
	 * @return El monto de dinero que el cliente debe pagar
	 * @throws Exception si no encuentra el carro
	 */
	public double atenderClienteSale (String matricula) throws Exception
	{
		Carro porSalir = sacarCarro(matricula);
		if (porSalir == null) {
			throw new Exception("No existe ningún carro con esa matricula.");
		}
		return cobrarTarifa(porSalir);
	}

	/**
	 * Busca un parqueadero co cupo dentro de los 8 existentes y parquea el carro
	 * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
	 * @return El parqueadero en el que qued� el carro
	 * @throws Exception
	 */
	public String parquearCarro(Carro aParquear) throws Exception
	{
		String rta = "";
		for (int i = 0; i < parqueaderos.size(); i++) {
			List<Carro> parAct = parqueaderos.get(i);
			if (parAct.size() < 4) {
				parAct.add(aParquear);
				rta = "PARQUEADERO " + (i+1);
			}
		}
		if (rta.equals("")) {
			throw new Exception("Todos los parqueaderos están llenos");
		}
		return rta;    	    
	}

	/**
	 * Itera sobre los ocho parqueaderos buscando uno con la placa ingresada
	 * @param matricula del vehiculo que se quiere sacar
	 * @return el carro buscado
	 */
	public Carro sacarCarro (String matricula)
	{
		Carro rta = null;
		for (int i = 0; i < parqueaderos.size() && rta == null; i++) {

			if (i == 0) {
				rta = sacarCarroP1(matricula);
			}
			else if (i == 1) {
				rta = sacarCarroP2(matricula);
			}
			else if (i == 2) {
				rta = sacarCarroP3(matricula);
			}
			else if (i == 3) {
				rta = sacarCarroP4(matricula);
			}
			else if (i == 4) {
				rta = sacarCarroP5(matricula);
			}
			else if (i == 5) {
				rta = sacarCarroP6(matricula);
			}
			else if (i == 6) {
				rta = sacarCarroP7(matricula);
			}
			else {
				rta = sacarCarroP8(matricula);
			}
		}
		return rta;
	}

	/**
	 * Saca un carro del parqueadero 1 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP1 (String matricula)
	{
		Carro rta = null;
		List<Carro> parqueadero = parqueaderos.get(0);
		for (int i = 0; i < parqueadero.size() && rta == null; i++) {
			Carro act = parqueadero.get(i);
			if (act.darIdentificador().equals(matricula)) {
				rta = act;
				parqueadero.remove(0);
				break;
			} else {
				parqueaderoTemporal.add(parqueadero.remove(0));
			}
		}
		for (int i = 0; i < parqueaderoTemporal.size(); i++) {
			parqueadero.add(parqueaderoTemporal.remove(0));
		}
		return rta;
	}

	/**
	 * Saca un carro del parqueadero 2 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP2 (String matricula)
	{
		Carro rta = null;
		List<Carro> parqueadero = parqueaderos.get(1);
		for (int i = 0; i < parqueadero.size() && rta == null; i++) {
			Carro act = parqueadero.get(i);
			if (act.darIdentificador().equals(matricula)) {
				rta = act;
				parqueadero.remove(0);
				break;
			} else {
				parqueaderoTemporal.add(parqueadero.remove(0));
			}
		}
		for (int i = 0; i < parqueaderoTemporal.size(); i++) {
			parqueadero.add(parqueaderoTemporal.remove(0));
		}
		return rta;
	}

	/**
	 * Saca un carro del parqueadero 3 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP3 (String matricula)
	{
		Carro rta = null;
		List<Carro> parqueadero = parqueaderos.get(2);
		for (int i = 0; i < parqueadero.size() && rta == null; i++) {
			Carro act = parqueadero.get(i);
			if (act.darIdentificador().equals(matricula)) {
				rta = act;
				parqueadero.remove(0);
				break;
			} else {
				parqueaderoTemporal.add(parqueadero.remove(0));
			}
		}
		for (int i = 0; i < parqueaderoTemporal.size(); i++) {
			parqueadero.add(parqueaderoTemporal.remove(0));
		}
		return rta;
	}
	/**
	 * Saca un carro del parqueadero 4 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP4 (String matricula)
	{

		Carro rta = null;
		List<Carro> parqueadero = parqueaderos.get(3);
		for (int i = 0; i < parqueadero.size() && rta == null; i++) {
			Carro act = parqueadero.get(i);
			if (act.darIdentificador().equals(matricula)) {
				rta = act;
				parqueadero.remove(0);
				break;
			} else {
				parqueaderoTemporal.add(parqueadero.remove(0));
			}
		}
		for (int i = 0; i < parqueaderoTemporal.size(); i++) {
			parqueadero.add(parqueaderoTemporal.remove(0));
		}
		return rta;
	}
	/**
	 * Saca un carro del parqueadero 5 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP5 (String matricula)
	{
		Carro rta = null;
		List<Carro> parqueadero = parqueaderos.get(4);
		for (int i = 0; i < parqueadero.size() && rta == null; i++) {
			Carro act = parqueadero.get(i);
			if (act.darIdentificador().equals(matricula)) {
				rta = act;
				parqueadero.remove(0);
			} else {
				parqueaderoTemporal.add(parqueadero.remove(0));
			}
		}
		for (int i = 0; i < parqueaderoTemporal.size(); i++) {
			parqueadero.add(parqueaderoTemporal.remove(0));
		}
		return rta;
	}
	/**
	 * Saca un carro del parqueadero 6 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP6 (String matricula)
	{

		Carro rta = null;
		List<Carro> parqueadero = parqueaderos.get(5);
		for (int i = 0; i < parqueadero.size() && rta == null; i++) {
			Carro act = parqueadero.get(i);
			if (act.darIdentificador().equals(matricula)) {
				rta = act;
				parqueadero.remove(0);
				break;
			} else {
				parqueaderoTemporal.add(parqueadero.remove(0));
			}
		}
		for (int i = 0; i < parqueaderoTemporal.size(); i++) {
			parqueadero.add(parqueaderoTemporal.remove(0));
		}
		return rta;
	}
	/**
	 * Saca un carro del parqueadero 7 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP7 (String matricula)
	{

		Carro rta = null;
		List<Carro> parqueadero = parqueaderos.get(6);
		for (int i = 0; i < parqueadero.size() && rta == null; i++) {
			Carro act = parqueadero.get(i);
			if (act.darIdentificador().equals(matricula)) {
				rta = act;
				parqueadero.remove(0);
				break;
			} else {
				parqueaderoTemporal.add(parqueadero.remove(0));
			}
		}
		for (int i = 0; i < parqueaderoTemporal.size(); i++) {
			parqueadero.add(parqueaderoTemporal.remove(0));
		}
		return rta;
	}
	/**
	 * Saca un carro del parqueadero 8 dada su matricula
	 * @param matricula del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP8 (String matricula)
	{

		Carro rta = null;
		List<Carro> parqueadero = parqueaderos.get(7);
		for (int i = 0; i < parqueadero.size() && rta == null; i++) {
			Carro act = parqueadero.get(i);
			if (act.darIdentificador().equals(matricula)) {
				rta = act;
				parqueadero.remove(0);
				break;
			} else {
				parqueaderoTemporal.add(parqueadero.remove(0));
			}
		}
		for (int i = 0; i < parqueaderoTemporal.size(); i++) {
			parqueadero.add(parqueaderoTemporal.remove(0));
		}
		return rta;
	}
	/**
	 * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
	 * la tarifa es de $25 por minuto
	 * @param car recibe como parametro el carro que sale del parqueadero
	 * @return el valor que debe ser cobrado al cliente 
	 */
	public double cobrarTarifa (Carro car)
	{
		Date fecha = new Date();
		long salida = fecha.getTime();
		double tiempo = salida - car.darLlegada();
		double rta = (tiempo/60000);
		return rta*25;	
	}
	
	public void estadoParqueadero()
	{
		String carro = "-##-";
		int c = 0;
		for (int i = 0; i < parqueaderos.size(); i++) {
			List<Carro> parAct = parqueaderos.get(i);
			System.out.println("-- Parqueadero " + (i+1) + " --");
			for (int j = 0; j < parAct.size(); j++) {
				System.out.println(carro + parAct.get(j).darIdentificador());
				c++;
			}
		}
		for (int i = 0; i < parqueaderoTemporal.size(); i++) {
			System.out.println("-- Parqueadero Temporal --");
			System.out.println(carro);
			c++;
		}
		System.out.println("Hay " + c + " Carros en el parqueadero.");
	}
	
	public void carrosEnCola()
	{
		for (int i = 0; i < carrosEnEspera.size(); i++) {
			System.out.println("-##-" + carrosEnEspera.get(i).darIdentificador());
		}
		System.out.println("Hay " + carrosEnEspera.size() + " carros en espera.");
	}
}
