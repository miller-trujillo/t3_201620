package estructuras;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

public class Cola<T extends IdentificadoUnicamente> extends ListaEncadenadaAbstracta<T>
{

    /**
     * Construye una lista vacia
     * post: se ha inicializado el primer nodo en null
     */
    public Cola()
    {
        primero = null;
        ultimo = null;
        size = 0;
    }

    /**
     * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
     * @param nPrimero el elemento a guardar en el primer nodo
     * @throws NullPointerException si el elemento recibido es nulo
     */
    public Cola(T nPrimero)
    {
        if(nPrimero == null)
        {
            throw new NullPointerException();
        }
        primero = new NodoListaSencilla<T>( nPrimero );
        ultimo = primero;
        size = 0;
    }

    /**
     * Agrega un elemento al final de la lista
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
     * @param elem el elemento que se desea agregar.
     * @return true en caso que se agregue el elemento o false en caso contrario. 
     * @throws NullPointerException si el elemento es nulo
     */
    public boolean add( T elem )throws NullPointerException
    {
        if(elem == null)
        {
            throw new NullPointerException( );
        }

        boolean agregado = false;
        if(primero == null)
        {
            primero = new NodoListaSencilla<T>( elem );
            ultimo = primero;
            System.out.println("Hola");
        }
        else
        {
        	NodoListaSencilla<T> nuevo = new NodoListaSencilla<T>(elem);
			ultimo.cambiarSiguiente(nuevo);
			ultimo = nuevo;
        }
        size++;
        return true;
    }

    public void add( int pos, T elem )  
    {
    	
    }


    public ListIterator<T> listIterator( )
    {
        throw new UnsupportedOperationException ();
    }


    public ListIterator<T> listIterator( int pos )
    {
        throw new UnsupportedOperationException ();
    }

    /**
     * Elimina el nodo que contiene al objeto que llega por par�metro
     * @param objeto el objeto que se desea eliminar. objeto != null
     * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
     */
    public boolean remove( Object objeto )
    {
        return true;
    }

    /**
     * Elimina el nodo en la posici�n por par�metro
     * @param pos la posici�n que se desea eliminar
     * @return el elemento eliminado
     * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
     */
    public T remove( int pos )
    {
        if (primero == null) {
			throw new NullPointerException();
		}
        T rta = primero.darElemento();
        primero = primero.darSiguiente();
        size--;
        return rta;
    }

    /**
     * Deja en la lista solo los elementos que est�n en la colecci�n que llega por par�metro
     * @param coleccion la colecci�n de elementos a mantener. coleccion != null
     * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
     */
    public boolean retainAll( Collection<?> coleccion )
    {
        // TODO Parte 3.A: Completar seg�n la documentaci�n X
        NodoListaSencilla<T> n = primero;
        boolean rta = false;
        while( n != null )
        {
            if( !coleccion.contains( n.darElemento( ) ) )
            {                   
                if( remove( n.darElemento( ) ) )
                {
                    rta = true;
                }
            }
            n = n.darSiguiente( );
        }
        return rta;
    }

    /**
     * Crea una lista con los elementos de la lista entre las posiciones dadas
     * @param inicio la posici�n del primer elemento de la sublista. Se incluye en la sublista
     * @param fin la posici�n del �tlimo elemento de la sublista. Se excluye en la sublista
     * @return una lista con los elementos entre las posiciones dadas
     * @throws IndexOutOfBoundsException Si inicio < 0 o fin >= size() o fin < inicio
     */
    public List<T> subList( int inicio, int fin )
    {
        if( inicio < 0 || fin < inicio)
        {
            throw new IndexOutOfBoundsException( );
        }
        List<T> lista = new Cola<T>( );
        NodoListaSencilla<T> n = primero;
        int c = 0;
        while( c <= fin && n != null )
        {
            if( c >= inicio && c <= fin )
            {
                add( n.darElemento( ) );
            }
            c++;
            n = n.darSiguiente( );
        }
        if( c != fin+1)
        {
            throw new IndexOutOfBoundsException( );
        }
        return lista;
    }
}
